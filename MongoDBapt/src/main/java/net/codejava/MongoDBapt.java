package net.codejava;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
//import com.mongodb.DB;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
//import com.mongodb.client.MongoIterable;
//import com.mongodb.MongoClient;

public class MongoDBapt {

	public static MongoClient mongoClient;
	public static MongoDatabase db;
	public static MongoCollection<Document> collection;
	
	
	public static void main(String[] args) {
		
		mongoClient = MongoClients.create();
		System.out.println("CONECCION SATISFACTORIA");
		
		//**************CREATE A MONGODB DATABASE
		db= mongoClient.getDatabase("MongoDB");
		System.out.println("CREACION DE UNA BASE DE DATOS SATISFACTORIA");
		
		//************CREATE A COLLECTION
		collection = db.getCollection("Cole");
		System.out.println("CREACION DE COLLECCION SATISFACTORIA");
		
		// Creating a documents and saving in a collection
		Documents document1= new Documents();
		document1.setname("ALICIA");
		document1.setemail("alicia.xxx@gmail.com");
		document1.settwitter("twitteralicia");
		document1.setcity("Arequipa55");
		document1.setzip(12345);
		Document createdDocument= new Document();
		createdDocument=document1.creatingDocuments();
		
		
		// Creating an object form Collection class
		Collection collections= new Collection();
		
		
		
		//********PRINT A SUMMARY OF THE MONGODB DOCUMENTS CREATED
		
		collections.savingDcoumentInCollection(collection, createdDocument);
		
		
		
		
		
		//Reading and printing list of documents from a collection
		collections.printingDocuments(collection);
		
		//Retrieving list of collections
		collections.retrievingListCollections(db);
		
		// 
		
		//Drop collection from database
		collections.dropCollection(db,collection);
		
		
		
		//********CLEAN THE MONGODB COLLECTION IS NOT EMPTY
		collections.deletingDocumentsfromCollection(collection);
	}
	
	
		

		

}
