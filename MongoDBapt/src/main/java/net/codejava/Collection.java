package net.codejava;

import org.bson.Document;

import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

public class Collection {
	
	public MongoCollection<Document> collection;
	public MongoDatabase db;
	public MongoIterable<String> list;
	
	
	// Reading and printing documents from a collection
	public void printingDocuments(MongoCollection <Document> collection ) {
	FindIterable<Document> result = collection.find();
	System.out.println("LISTA DE DOCUMENTOS MONGODB CREADOS");

	for (Document document:result) {
	    System.out.println(document.toJson());
		}
	}
	// Saving document in a collection
	public MongoCollection <Document>   savingDcoumentInCollection(MongoCollection <Document> collection , Document document) {
		//FindIterable<Document> result = collection.find();
		collection.insertOne(document);
		return collection;
	}
	// Retrieving the list of the collections	
	public void retrievingListCollections(MongoDatabase db){
		 list = db.listCollectionNames();
	      System.out.println("LISTA DE COLLECCIONES:");
	      for (String name : list) {
	         System.out.println(name);
	      }	
	}
	//Drop a specific collection
	public void dropCollection(MongoDatabase db,MongoCollection <Document> collection) {
	//db.getCollection(collection).drop();
	collection.drop();
    System.out.println("COLLECION BORRADA SATISFACTORIAMENTE");
    System.out.println("LISTA DE COLLECCIONES DESPUES DE BORRAR UNA COLLECCION ESPECIFICA");
    for (String name : list) {
       System.out.println(name);
    }
    }
	//Deleting all documents from a collection
	public void deletingDocumentsfromCollection(MongoCollection <Document> collection) {
		if(collection!=null) {
			//collection.deleteMany(null);
			collection.deleteMany(new Document());
		}
		//collection.re
			System.out.println("COLLECCION VACIA");
		
	}
}
			
