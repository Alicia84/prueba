package net.codejava;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import org.bson.Document;

public class Documents {


		private String name;
		private String email;
		private String twitter;
		private String city;
		private int zip;
		
		public Documents() {
			
		}
		
		
		
		public String getname() {
			return name;
		}
		
		public String getemail() {
			return email;
		}
		
		public String  gettwitter() {
			return twitter;
		}
		
		public String  getcity() {
			return city;
		}
		
		public int getzip() {
			return zip;
		}
		
		
		
		
		public void setname(String name) {
			this.name=name;
			
		}
		public void setemail(String email) {
			this.email=email;
			
		}
		public void settwitter(String twitter) {
			this.twitter=twitter;
			
		}
		public void setcity(String city) {
			this.city=city;
			
		}
		
		public void setzip(int zip) {
			this.zip=zip;
			
		}
		
		
		// Creating a document	
		public  Document creatingDocuments () {
			ArrayList<String> myHobbies = new ArrayList<String>();
			  
			  myHobbies.add("music");
			  myHobbies.add("movies");
			  
			  Document document = new Document("name",getname()).
			    append("email", getemail()).
			    append("twitter", gettwitter()).
			    append("hobbies", myHobbies).
			    append("location", new Document("city", getcity())).append("zip", getzip());
			  
			  return document;	  
		
		}
		
	
	}


